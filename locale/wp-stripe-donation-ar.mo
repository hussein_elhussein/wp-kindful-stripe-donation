��    j      l  �   �      	     	     	     	     	     	  
   &	     1	     9	     G	     S	     a	     u	     |	     �	     �	     �	     �	  ,   �	  :   
     Z
     c
     t
  
   �
     �
     �
  
   �
     �
     �
     �
     �
     �
     �
               &     5     I     \     i     |     �     �     �  ,   �     �                    #     5     O  
   n     y     �     �     �     �     �     �  	   �     �               "     0     9     H     U     h     n     z     �     �     �     �  #   �  !        ?  	   K     U  
   l     w     �     �     �  	   �     �     �     �     �     �               !     9     I     ^     }     �     �     �     �  %   �       	     �   (     �     �     �     �                     /     L     `  5   v     �     �  '   �     �  ,     '   C  ,   k  _   �     �            '   9     a     w      �  
   �      �     �     �  1   �     (     E     T     d  %   z     �     �  %   �  "   �  ;        W     q  a   �  ,   �       !   ,  :   N     �     �  '   �     �  #   �     !  
   A     L  "   ^     �     �     �  %   �     �  
        &     A  !   S     u  @   �     �     �  D   �  M   7  $   �  +   �  -   �  >     /   C  &   s     �  )   �  &   �  +   �     )      ;     \     n     �      �  "   �     �     �            *        H  .   f  %   �     �     �     �       &   *  +   Q  
   }     �     i          X       Z      +              g   <   /   O   "      1       W   )      V   *   @                             G   U   T          !   	      b   \   5       %                     H      f       &           J   =   9   ^   6   e             '   4   .   #           A          -   M   :               E   P       ;   `      [   2              S          I       Y          
             j   B   L              a   >      d       (   Q   ]   D          h       c      3       ?   7      $   ,      _              K   F       0   8   R   C   N                10 100 20 50 Actions Add Amount Address Address Label All Amounts All Donations Allow custom amount Amount Amount already exists Amount is created successfully Amount is not valid Amount not found Amount updated successfully An error occurred during deleting the amount By clicking submit, you are agreeing to submitting payment Campaign Campaign Options Card Agreement Card Label Choose Your Amount City City Label Country Country Label Credit Card Details Currency Custom Amount Label Custom Amount is not valid Date Delete Amount Display Header Donate Amount Label Donate Button Text Donation For Donation For Label Donation Form Banner Donation Info Email Donation Options Donation Title Donation information will send to this email Donation not found Edit Amount Email Email Label Enter Your Amount Failed to create donation Failed to update product price First Name First Name Label General Settings In memory of In memory of field Invalid payload Invalid signature Key Settings Last Name Last Name Label List of all donations Monthly Monthly Label One Time One Time Label Other Amount Payment Modal Logo Phone Phone Label Please Enter Email Please Enter Valid Email Please Enter Your Address Please Enter Your First Name Please Enter Your Last Name Please fill all the required fields Please select an amount to donate Private Key Recurring Recurring is not valid Secret Key Secret key missing Select a Template Select a banner Select a logo Shortcode State State Label Stripe Product ID Submit Template Settings Thank you for your donation USD Update General Settings Update Settings Update Temp Settings Use comma "," separated values WP Stripe Donation WP Stripe Donation Key Settings WP Stripe General Settings WP Stripe Template Settings Webhooks Key Your information updated successfully ZIP ZIP Label Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Project-Id-Version: 
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
MIME-Version: 1.0
Language: ar
X-Generator: Poedit 2.4.1
 10 100 20 50 إجراءات اضف مبلغ العنوان اسم حقل العنوان كل المبالغ كل التبرعات السماح للمستخدم بإختيار مبلغ المبلغ المبلغ موجود تم إضافة المبلغ بنجاح المبلغ غير صحيح لم يتم العثور على المبلغ تم تحديث المبلغ بنجاح حصل خطأ اثناء حذف المبلغ من خلال الضغط على تبرع الآن, إنك توافق على دفع المبلغ الحملة خيارات الحملة اتفاقية الدفع عنوان حقل بطاقة الدفع إختر المبلغ المدينة عنوان حقل المدينة البلد عنوان حقل المدينة بطاقة الدفع العملة عنوان حقل المبلغ الإحتياري المبلغ غير صحيح التاريخ حذف مبلغ عرض الواجهة عنوان حقل تبرع بمبلغ نص زر تبرع بمبلغ تبرع لصالح عنوان حقل تبرع لصالح رأس استمارة التبرع العنوان البريدي لمعلومات التبرع خيارات التبرع عنوان التبرع بيانات التبرع سترسل إلى عنوان البريد الإلكترةوني هذا لم يتم العثور على التبرع تعديل مبلغ البريد الإلكتروني اسم حقل عنوان البريد الإلكتروني أدخل المبلغ فشل في إضافة تبرع تعذر تحديث سعر المنتج الأسم الأول اسم حقل الأسم الأول الإعدادات العامة لذكرة حقل لذكرة البيانات غير صحيحة التوقيع غير صحيح إعدادات المفاتيح الإسم الثاني اسم حقل الإسم الثاني عرض كل التبرعات شهريا عنوان زر شهريا مرة واحدة عنوان زر مرة واحدة مبلغ آخر صورة لإستمارة الدفع للإطار المنبثق الهاتف عنوان حقل الهاتف الرجاء إدخال عنوان البريد الإلكتروني الرجاء إدخال عنوان البريد الإلكتروني صحيح الرجاء إدخال عنوانك الرجاء إدخال اسمك الأول الرجاء إدخال اسم العائلة الرجاء تعبئة كافة الحقول المطلوبة الرجاء إختيار مبلغ للتبرع المفتاح المشفر الخاص دفع متكرر المبلغ الشهري ليس صالح المفتاح المشفر السري المفتاح السري غير موجود إختر قالب إختر شعار البداية إختر شعار كود مختصر الولاية عنوان حقل الولاية معرف سترايب للمنتج تبرع الآن إعدادات القالب شكرا للتبرع د.أ تحديث الإعدادات العامة تحديث الإعدادات تحديث الإعدادات الموءقتة ضع فاصلة "," بين القيم WP Stripe Donation إعدادات المفتايح الإعدادات العامة إعدادات القالب مفتاح إشعارات سترايب تم تعديل معلوماتك بنجاح الرمز عنوان حقل الرمز 