<?php

/**
 * Plugin Name: 	WP Stripe Donation
 * Text Domain:   wp-stripe-donation
 * Description: 	This WordPress Stripe Donation is a simple plugin that allows you to collect donations on your website 
 *                via Stripe payment method and send the donation info to Kindful.
 */

if (!defined('WPINC')) {
    die;
}
if (!defined('ABSPATH')) {
    exit;
}

global $wpdb;

define('WPSD_PATH', plugin_dir_path(__FILE__));
define('WPSD_ASSETS', plugins_url('/assets/', __FILE__));
define('WPSD_LANG', plugins_url('/languages/', __FILE__));
define('WPSD_SLUG', plugin_basename(__FILE__));
define('WPSD_PRFX', 'wpsd_');
define('WPSD_CLS_PRFX', 'cls-wpsd-');
define('WPSD_TXT_DOMAIN', 'wp-stripe-donation');
define('WPSD_VERSION', '1.4.1');
define('WPSD_TABLE', $wpdb->prefix . 'wpsd_stripe_donation');
define('WPSD_TABLE_AMOUNT', $wpdb->prefix . 'wpsd_stripe_amounts');

// load dependencies:
if ( is_readable( __DIR__ . '/vendor/autoload.php' ) ) {
	require __DIR__ . '/vendor/autoload.php';
}
// boostrap the plugin:
require_once WPSD_PATH . 'inc/' . WPSD_CLS_PRFX . 'master.php';
$wpsd = new Wpsd_Master();
register_activation_hook(__FILE__, array($wpsd, WPSD_PRFX . 'install_tables'));
$wpsd->wpsd_run();
register_deactivation_hook(__FILE__, array($wpsd, WPSD_PRFX . 'unregister_settings'));